from tkinter import *

def newProject():
    print("New project Menu Clicked")
def openFile():
    print("Open file Menu Clicked")
def close():
    print("Close Menu Clicked")
def undo():
    print("Undo Menu Clicked")

root = Tk()
mymenu = Menu(root)
root.config(menu=mymenu)

submenu = Menu(mymenu)
mymenu.add_cascade(label="File", menu=submenu)
submenu.add_cascade(label="New Project", command=newProject)
submenu.add_cascade(label="Open File", command=openFile)
submenu.add_separator()
submenu.add_cascade(label="Exit", command=close)

newmenu = Menu(mymenu)
mymenu.add_cascade(label="Edit", menu=newmenu)
newmenu.add_cascade(label="Undo", command=undo)

root.mainloop()
