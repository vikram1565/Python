from tkinter import *


class demo():

    def __init__(self, rootnode):
        frame = Frame(rootnode)
        frame.pack()

        self.button1 = Button(frame, text="click me", command=self.test)
        self.button1.pack()

        self.button2 = Button(frame, text="close", command=frame.quit)
        self.button2.pack()

    def test(self):
        print("Button Clicked !!!")


root = Tk()
d = demo(root)

root.mainloop()
