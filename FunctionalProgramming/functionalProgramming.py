# Functional programming is a general concept which states that the code should be contained in functions.
# Functional programming allows us to reuse the code as many number of times we want.
# It not only makes the code efficient but also maintainable.
# Functional programming can be implemented by the use of functions in our code.
#  Functional programming means deviding your code into small functions and
# pass one function as argument to another function

#  add function add 10 into a


def add(a):
    return a+10

# test function is called two times


def test(func, arg):
    return func(func(arg))


# call to test function
print(test(add, 10))
