# Map allows us to perfrom single operation on list or operate on a list.

# Map Example


def add(z):
    return z+5


myList = [10, 20, 30, 40, 50]

updatedlist = list(map(add, myList))
print("Without Lambda: ", updatedlist)


# use of lambda
myList = [10, 20, 30, 40, 50]

updatedlist = list(map(lambda x: x+5, myList))
print("With Lambda: ", updatedlist)
