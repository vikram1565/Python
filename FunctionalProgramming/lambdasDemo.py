
# lambda contain a variable and variable have a expression.
# lambda is a anonymous function because they are a function with no name.
# lambda not include return statement because it always contains a expression which is returned.
# Lambda functions are mainly used in combination with the functions filter(), map() and reduce().
# The general syntax of a lambda function is quite simple:lambda argument_list: expression
# lambda function to find out cube of a number.

print((lambda a: a**3)(3))
