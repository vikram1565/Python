# Generators are same like list, tuples
# Generators do not allow indexing like list but it is iterated by using for loop
# yield used to create generator

# Create generator program


def function():
    counter = 0
    while counter < 5:
        yield counter
        counter += 1


for x in function():
    print(x)


# Generator to create even number list
def even(x):
    for i in range(x):
        if i % 2 == 0:
            yield i


a = input("Enter any Number: ")
b = int(a)
print(list(even(b)))
