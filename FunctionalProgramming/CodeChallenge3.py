# Consider a list in Python which includes prices of all the items in a store.
# Build a function to discount the price of all the products by 10 % .
# Use map to apply the function to all the elements of the list so that all the product prices are discounted.

priceList = [100, 200, 300, 400, 500]

# using function


def discount(x):
    return x - (x*10) / 100


print("Using funtcion : ", list(map(discount, priceList)))

# using lambdas

print("Using lambdas : ", list(map(lambda x: x - (x*10) / 100, priceList)))
