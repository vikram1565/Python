from itertools import count, accumulate, takewhile

# count function from itertools modules

for i in count(2):
    print(i)
    if (i >= 20):
        break

""" Output :
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
"""
# accumulate function
# keep adding one no into another no

print(list(accumulate(range(8))))

""" Output:
    [0, 1, 3, 6, 10, 15, 21, 28]
"""
# take while function
no = list(accumulate(range(8)))
print(list(takewhile(lambda x: x <= 6, no)))

""" Output :
[0, 1, 3, 6]
"""
