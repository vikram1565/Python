# Inheritance

# declare a Employee class


class Employee:

    # initialise Employee Object Fields

    def __init__(self, EmpName, EmpAddress, EmpSalary, EmpPhone):
        self.EmpName = EmpName
        self.EmpAddress = EmpAddress
        self.EmpSalary = EmpSalary
        self.EmpPhone = EmpPhone

    # Get Employee Data From User

    def getEmployeeData(self):
        self.EmpName = input('Enter Employee Name : ')
        self.EmpAddress = input('Enter Employee Address : ')
        self.EmpSalary = input('Enter Employee Salary : ')
        self.EmpPhone = input('Enter Employee Phone : ')

    # Display Employee Data

    def displayEmployeeData(self):
        print('Employee Name : ', self.EmpName)
        print('Employee Address : ', self.EmpAddress)
        print('Employee Salary : ', self.EmpSalary)
        print('Employee Phone : ', self.EmpPhone)

# here company class is inherited from Employee class


class company(Employee):
    def __init__(self, companyName):
        self.companyName = companyName

    def getCompanyData(self):
        self.companyName = input('Enter Company Name: ')

    def displayCompanyData(self):
        print('------------------- Employee Details ------------------------')
        print('Compnay Name : ', self.companyName)

# Create Company Class Object
# Initialize object require some default data.. if you don't pass any data then it gives error


c = company("")


# Call Methods
c.getCompanyData()
c.getEmployeeData()
c.displayCompanyData()  # call to inherited methods
c.displayEmployeeData()  # call to inherited methods
