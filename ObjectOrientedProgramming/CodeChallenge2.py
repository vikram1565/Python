# Statement :
"""
Using the concept of operator overloading in Python, 
change the behavior of the multiplication operator in a way that multiplication operator behaves 
like an addition operator.
"""


class test:

    def __init__(self, x):
        self.x = x

    def __mul__(self, other):  # change the behavior  of * operator to + operator
        x = self.x + other.x
        return x


t1 = test(5)
t2 = test(5)

print(t1 * t2)

# output : 10
