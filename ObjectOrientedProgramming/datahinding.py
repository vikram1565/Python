# __ symbol used to hide the value from external user


class demo:
    __hiddenVar = 10

    def add(self, x):
        self.__hiddenVar += x
        print("value is : ", self.__hiddenVar)


d = demo()
d.add(20)


# hidden variable access error : print(d.__hiddenVar) = > 'demo' object has no attribute '__hiddenVar'
""" Output:
value is :  30

"""
