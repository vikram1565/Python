class addition:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __add__(self, other):
        a = self.a + other.a
        b = self.b + other.b
        return a + b


aObj = addition(10, 20)
bObj = addition(10, 20)
print("+ operator value is : ", aObj + bObj)


"""Output:
+ operator value is :  60
"""
