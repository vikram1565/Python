# recursion function to calculate factorial of a number


def calculateFactorial(n):
    if (n == 1):
        return 1
    else:
        # Recursive call to calculateFactorial
        return n * calculateFactorial(n-1)


no = input('Enter any number : ')
num = int(no)
print("Factorial of " + no + " is : ", calculateFactorial(num))


""" Output :
    Enter any number : 5
    Factorial of 5 is :  120
"""
