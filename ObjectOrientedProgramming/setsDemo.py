# Sets have unique elements

setData = {1, 2, 3, 4, 5, 5}
print('Set Data : ', setData)  # Display only unique data

# Add

setData.add(6)
print('after add : ', setData)

# remove

setData.remove(5)
print('after remove : ', setData)

# union
setA = {1, 2, 3, 4}
setB = {1, 3, 5, 7}

print('after union : ', setA | setB)

# intersection

print('after intersection : ', setA & setB)

# difference
print('after setA - setB difference : ', setA - setB)
print('after setB - setA difference : ', setB - setA)
