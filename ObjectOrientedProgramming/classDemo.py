
# declare a Employee class


class Employee:

    # initialise Employee Object Fields

    def __init__(self, EmpName, EmpAddress, EmpSalary, EmpPhone):
        self.EmpName = EmpName
        self.EmpAddress = EmpAddress
        self.EmpSalary = EmpSalary
        self.EmpPhone = EmpPhone

    # Get Employee Data From User

    def getEmployeeData(self):
        self.EmpName = input('Enter Employee Name : ')
        self.EmpAddress = input('Enter Employee Address : ')
        self.EmpSalary = input('Enter Employee Salary : ')
        self.EmpPhone = input('Enter Employee Phone : ')

    # Display Employee Data

    def displayEmployeeData(self):
        print('------------------- Employee Details ------------------------')
        print('Employee Name : ', self.EmpName)
        print('Employee Address : ', self.EmpAddress)
        print('Employee Salary : ', self.EmpSalary)
        print('Employee Phone : ', self.EmpPhone)

# Create Employee Class Object
# Initialize object require some default data.. if you don't pass any data then it gives error


emp = Employee("", "", "", "")


# Call Employee Class Methods

emp.getEmployeeData()
emp.displayEmployeeData()


# Output :

"""Enter Employee Name: Vikram
Enter Employee Address: Pune
Enter Employee Salary: 30000
Enter Employee Phone: 9766984458
------------------- Employee Details - -----------------------
Employee Name:  Vikram
Employee Address:  Pune
Employee Salary:  30000
Employee Phone:  9766984458"""
