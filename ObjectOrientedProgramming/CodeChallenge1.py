
# Statement :
""" Using the concept of object oriented programming and inheritance, create a super class named Computer, which has two sub classes named Desktop and Laptop.
Define two methods in the Computer class named getspecs and displayspecs, to get the specifications and display the specifications of the computer.
You can use any specifications which you want.
The Desktop class and the Laptop class should have one specification which is exclusive to them for example laptop can have weight as a special specification.
Make sure that the sub classes have their own methods to get and display their special specification.
Create an object of laptop/ desktop and make sure to call all the methods from the computer class as well as the methods from the own class 
"""
#Solution

#create class Computer

class computer:
    def __init__(self, weight,hd):
        self.weight = weight
        self.hd = hd
    def getspecs(self):
        self.weight = input('Enter weight of computer : ')
        self.hd = input('Enter hard disk : ')
    def displayspecs(self): 
        print('Details are as : ')
        print('Weight is : ', self.weight)
        print('Hard Disk is : ', self.hd)

# create desktop class which is the base class of computer

class desktop(computer):
    def __init__(self, ram):
        self.ram = ram
    def getdesktopspecs(self):
        self.ram = input('Enter desktop ram : ')
    def displaydesktopspecs(self):
        print('Desktop ram is : ', self.ram)

# create laptop class which is the base class of computer

class laptop(computer):
    def __init__(self, color):
        self.color = color

    def getlaptopspecs(self):
        self.color = input('Enter laptop color : ')

    def displaylaptopspecs(self):
        print('Laptop color is : ', self.color)

# create desktop/laptop object and call the methods

lap = laptop('')
lap.getspecs()
lap.getlaptopspecs()
lap.displayspecs()
lap.displaylaptopspecs()


# Output -
"""
Enter weight of computer : 15Kg
Enter hard disk : 1TB
Enter laptop color : Blue
-------------------------------------------
Details are as :
Weight is :  15Kg
Hard Disk is :  1TB
Laptop color is :  Blue

"""
