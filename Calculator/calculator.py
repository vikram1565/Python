# creating a normal calculator

from tkinter import *
import parser
root = Tk()
root.title('Calculator')

# get user input and assign to inputbox
i = 0


def getInput(num):
    global i
    inputbox.insert(i, num)
    i += 1


def getOperator(opt):
    global i
    length = len(opt)
    inputbox.insert(i, opt)
    i += len(opt)


def clearinputbox():
    inputbox.delete(0, END)


def deleteSingleElement():
    str = inputbox.get()
    if len(str):
        newStr = str[:-1]
        clearinputbox()
        inputbox.insert(0, newStr)
    else:
        clearinputbox()
        inputbox.insert(0, "Error")


def factorial():
    str = inputbox.get()
    if len(str):
        exp = str[:-1]
        ans = parser.expr(exp).compile()
        result = eval(ans)
        result = calculateFactorial(int(result))
        clearinputbox()
        inputbox.insert(0, result)


def calculateFactorial(n):
    if (n == 1):
        return 1
    else:
        # Recursive call to calculateFactorial
        return n * calculateFactorial(n-1)


def calculateResult():
    exp = inputbox.get()
    s = "!"
    if exp.find(s) == -1:
        try:
            ans = parser.expr(exp).compile()
            result = eval(ans)
            clearinputbox()
            inputbox.insert(0, result)
        except Exception:
            clearinputbox()
            inputbox.insert(0, "Error")
    else:
        factorial()


# adding the input field
inputbox = Entry(root, bg="Pink")
inputbox.grid(row=1, columnspan=6, sticky=W+E)

# adding buttons
Button(root, text="1", command=lambda: getInput(1), height=1,
       width=2, bg="lime green").grid(row=2, column=0)
Button(root, text="2", command=lambda: getInput(2), height=1,
       width=2, bg="lime green").grid(row=2, column=1)
Button(root, text="3", command=lambda: getInput(3), height=1,
       width=2, bg="lime green").grid(row=2, column=2)

Button(root, text="4", command=lambda: getInput(4), height=1,
       width=2, bg="lime green").grid(row=3, column=0)
Button(root, text="5", command=lambda: getInput(5), height=1,
       width=2, bg="lime green").grid(row=3, column=1)
Button(root, text="6", command=lambda: getInput(6), height=1,
       width=2, bg="lime green").grid(row=3, column=2)

Button(root, text="7", command=lambda: getInput(7), height=1,
       width=2, bg="lime green").grid(row=4, column=0)
Button(root, text="8", command=lambda: getInput(8), height=1,
       width=2, bg="lime green").grid(row=4, column=1)
Button(root, text="9", command=lambda: getInput(9), height=1,
       width=2, bg="lime green").grid(row=4, column=2)

# adding other buttons
Button(root, text="AC", command=lambda: clearinputbox(), height=1,
       width=2, fg="red", bg="yellow").grid(row=5, column=0)
Button(root, text="0", command=lambda: getInput(0),
       height=1, width=2, bg="yellow").grid(row=5, column=1)
Button(root, text="=", command=lambda: calculateResult(),
       height=1, width=2, bg="yellow").grid(row=5, column=2)

Button(root, text="+", command=lambda: getOperator("+"),
       height=1, width=2, bg="blue").grid(row=2, column=3)
Button(root, text="-", command=lambda: getOperator("-"),
       height=1, width=2, bg="blue").grid(row=3, column=3)
Button(root, text="*", command=lambda: getOperator("*"),
       height=1, width=2, bg="blue").grid(row=4, column=3)
Button(root, text="/", command=lambda: getOperator("/"),
       height=1, width=2, bg="yellow").grid(row=5, column=3)

Button(root, text="pi", command=lambda: getOperator("*3.14"),
       height=1, width=2, bg="blue").grid(row=2, column=4)
Button(root, text="%", command=lambda: getOperator("%"),
       height=1, width=2, bg="blue").grid(row=3, column=4)
Button(root, text="(", command=lambda: getOperator(
    "("), height=1, width=2, bg="blue").grid(row=4, column=4)
Button(root, text="exp", command=lambda: getOperator("**"),
       height=1, width=2, bg="yellow").grid(row=5, column=4)

Button(root, text="<-", command=lambda: deleteSingleElement(),
       height=1, width=2, fg="red", bg="yellow").grid(row=2, column=5)
Button(root, text="!", command=lambda: getOperator("!"),
       height=1, width=2, bg="yellow").grid(row=3, column=5)
Button(root, text=")", command=lambda: getOperator(")"),
       height=1, width=2, bg="yellow").grid(row=4, column=5)
Button(root, text="^2", command=lambda: getOperator("**2"),
       height=1, width=2, bg="yellow").grid(row=5, column=5)

root.mainloop()
