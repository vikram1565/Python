# caret(^) and doller($) meta character demo
# this characters are used to specify start and end of a string
# caret for start and doller for end of a string

import re

pattern = r"^gr.y$"
if re.match(pattern, 'geey'): # here string must be start with 'gr' and end with 'y'. ow match fun will return false
    print('Matched')
else:
    print('Not match')