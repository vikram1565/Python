# star (*) meta character allow zero or more repetition of previous things i.e class, group ,single character

import re

pattern = r"helloworld*"

""" True condition """

if re.match(pattern, "helloworldworldsddscds"):
    print('matched')
else:
    print('not matched')

""" False condition """

if re.match(pattern, "worldsddscds"):
    print('matched')
else:
    print('not matched')
