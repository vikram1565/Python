import re

# group declared in between () parentheses

pattern = r"bat(ball)*bat"

# True condition

if re.match(pattern,"batballballballbat"): # here ball can be comes zero or n no of times
    print('True')
else:
    print('False')

# false condition

if re.match(pattern, "ballballballbat"): # ball group must in between bat  
    print('True')
else:
    print('False')
